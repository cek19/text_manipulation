
def read_txt():
    with open('example_text.txt','r') as f:
        read_data = f.readline()

    return read_data

if __name__ == "__main__":
    print(read_txt())

